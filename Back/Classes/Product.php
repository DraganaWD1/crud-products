<?php

require_once 'Connection/Connection.php';

use Connection;

class Product 
{
    public $db;

    public function __construct()
    {
        $this->db = new Connection;
    }
    public static function index(){
        $self = new self();
        $products = $self->db->prepare('SELECT * FROM products');
        $products->execute();
        $products = $products->fetchAll(PDO::FETCH_ASSOC);

        http_response_code(201);
        echo json_encode($products);
    }

    public function create($data){
        $product = $this->db->prepare('INSERT INTO products (name, description, quantity)
        VALUES (:name, :description, :quantity)');

        $product->bindParam(':name', $data['name']);
        $product->bindParam(':description', $data['description']);
        $product->bindParam(':quantity', $data['quantity']);

        if($product->execute()){
            http_response_code(201);
            echo json_encode(['message' => 'Product successfully created',  'success' => 1]);
            return;
        }

        http_response_code(404);
        echo json_encode(['message' => 'Sth went wrong',  'success' => 0]);
    }

    public static function update($data){
        $self = new self();

        $sql = "UPDATE products SET name=:name, description=:description, quantity=:quantity WHERE id=:id";
        $stmt= $self->db->prepare($sql);
        
        if($stmt->execute($data)){
            http_response_code(201);
            echo json_encode(['message' => 'Product successfully created', 'success' => 1]);
            return;
        }

        http_response_code(404);
        echo json_encode(['message' => 'Sth went wrong', 'success' => 0]);
    } 

    public static function show($id){
        $self = new self();
        $product = $self->db->prepare('SELECT * FROM products WHERE id = :id');
        $product->bindParam(':id', $id);
        $product->execute();
        $product = $product->fetchAll(PDO::FETCH_ASSOC);

        http_response_code(201);
        echo json_encode($product);
    }

    public static function delete($id){
        $self = new self();
        $product = $self->db->prepare('DELETE FROM products WHERE id = :id');
        $product->bindParam(':id', $id);
        if($product->execute()){
            http_response_code(201);
            echo json_encode(['message' => 'Product successfully deleted', 'success' => 1]);
            return;
        }

        http_response_code(404);
        echo json_encode(['message' => 'Sth went wrong', 'success' => 0]);
    }   
}