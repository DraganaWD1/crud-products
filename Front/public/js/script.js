$(document).ready(function(){
    $('#login').on('click', (e) => {
        e.preventDefault();
        let params = {
            email: $('#email').val(),
            password: $('#password').val()
        }

        if(validation(params)){
            loginUser(params);
            return;
        }
        //show errors
    })

    function loginUser(params){
        $.post('../Back/login.php', params).then(data => {
            if(data){
                let email = JSON.parse(data).email;

                window.location = 'dashboard.html';
            }
        })
    }

    function validation(params){
        if(!params.email.length || !params.password.length){
            return false;
        }

        return true;
    }

    
});